use clap::Parser;
use libc::ioperm;
use std::arch::asm;

#[derive(Parser)]
struct Args {
    /// Fan speed 0-255.  Omit for auto.
    speed: Option<u8>,
}

// unsafe fn inb(port: u16) -> u8 {
//     let value: u8;
//     asm!("in al, dx", in("dx") port, out("al") value, options(nomem, nostack));
//     value
// }

unsafe fn outb(value: u8, port: u16) {
    asm!("out dx, al", in("dx") port, in("al") value, options(nomem, nostack));
}

unsafe fn ec_write(addr: u16, val: u8, reg_addr: u16, reg_val: u16) {
    outb(0x2e, reg_addr);
    outb(0x10, reg_val);
    outb(0x2f, reg_addr);
    outb(addr.to_le_bytes()[0], reg_val);
    outb(0x2e, reg_addr);
    outb(0x11, reg_val);
    outb(0x2f, reg_addr);
    outb(addr.to_le_bytes()[1], reg_val);

    // write the fan speed...
    outb(0x2e, reg_addr);
    outb(0x12, reg_val);
    outb(0x2f, reg_addr);
    outb(val, reg_val);
}

fn main() {
    const WM2_ECRAM: u64 = 0x4e;
    const WM2_ECRAM_FAN_MANUAL: u16 = 0x275;
    const WM2_ECRAM_FAN_MANUAL_SPEED: u16 = 0x1809;
    const REG_ADDR: u16 = 0x4e;
    const REG_VAL: u16 = 0x4f;

    let args = Args::parse();

    unsafe {
        if ioperm(WM2_ECRAM, 2, 1) != 0 {
            eprintln!("permission denied");
            return;
        }
        match args.speed {
            None => {
                ec_write(WM2_ECRAM_FAN_MANUAL, 0, REG_ADDR, REG_VAL);
            }
            Some(x) if x <= 184 => {
                ec_write(WM2_ECRAM_FAN_MANUAL, 1, REG_ADDR, REG_VAL);
                ec_write(WM2_ECRAM_FAN_MANUAL_SPEED, x, REG_ADDR, REG_VAL);
            }
            _ => eprintln!("speed must be in range [0, 184]"),
        }
    }
}
